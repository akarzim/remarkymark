# Remarkymark

A convenient Middleman app for indexing, styling, navigating & displaying your
[Remark.js](http://remarkjs.com/) slideshows!

## How to install ?

### The Ruby way

Clone this Git repository, then step into the directory and run `bundle`.

### The Docker way

Build the container using `docker build -t remarkymark .`.

## How to run the server ?

### The Ruby way

Once installed, step into the directory and run `bundle exec middleman`.

### The Docker way

Once built, step into the directory and run `docker run -p 4567:4567 remarkymark`.

## How to get it work in the browser ?

Navigate to `http://localhost:4567` to reveal an index of all of your Remark.js
slideshows.

## How to create a new keynote ?

1. step into the directory
1. then create a new git branch named `draft/example`
1. make a new remark file, e.g. `/source/remarks/example.remark`
1. when you're done, push your work
1. and create a new Merge Request.

## What is a .remark file ?

It is just a Markdown file with some conventions. For more precisions, please
refer to the [Remark Markdown authoring guide](https://github.com/gnab/remark/wiki/Markdown).

## What about a custom stylesheet ?

Add `stylesheet: STYLESHEET_NAME` to the frontmatter of your `.remark` file to
link a custom stylesheet (Middleman Remark defaults to the `default` stylesheet
otherwise).

## What about a custom layout ?

Add `layout: LAYOUT_NAME`ot the frontmatter of your `.remark` file to link a
custom layout (Middleman Remark defaults to the `remark` layout otherwise).

## How to print my slides as a PDF file ?

A dockerized tool named [DeckTape] works like a charm for this purpose!
Just run this command and change `<SLIDES>` for the name of your set of slides:

```
docker run --rm -t --net=host -v `pwd`/public/remarks:/slides astefanutti/decktape http://localhost:4567/remarks/<SLIDES>.remark <SLIDES>.pdf
```

Note: You may have to use `host.docker.internal` instead of `localhost` on macOS
and Windows.

Tip: The generated PDF file may be huge, feel free to slim it with a tool like
[PDF24](https://tools.pdf24.org/fr/compresser-pdf) for example.

[DeckTape]: https://github.com/astefanutti/decktape

## Misc

### Thumbnails

Thumbnails for the homepage will automatically be used if they exist in
`/source/images/<SLIDES>.png` where `<SLIDES>` is the name of your set of
slides.

### HTML snippets

If an HTML bloc code containing `<head>` is present in your slides, Remark.js
will inject some code inside of it.

A workaround is to use similar unicode character in your snippet like
`<һead></һead>` (Cyrillic Small Letter Shha) or `<нead></нead>` (Cyrillic Small
Letter En) or some more fancy ones `<ℌead></ℌead>` or `<ℍead></ℍead>`.
