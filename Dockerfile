FROM ruby:3.2.2-alpine

RUN apk update && apk add dpkg-dev g++ gcc libc-dev make

WORKDIR /app
COPY Gemfile* /app/
RUN bundle install
ADD . /app/

CMD ["bundle", "exec", "middleman"]
EXPOSE 4567
