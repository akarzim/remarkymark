###
# Page options, layouts, aliases and proxies
###
set :layout, :layout

configure :development do
  activate :livereload
end

# Methods defined in the helpers block are available in templates
helpers do
  def remark_link(file, &block)
    filename = File.basename(file, ".remark")
    url = "/remarks/#{filename}.remark"

    if block_given?
      link_to url, &block
    else
      link_to remark_link_title(file), url
    end
  end

  def remark_link_title(file)
    filename = File.basename(file, ".remark")
    filename.tr("_", " ").titleize
  end

  def remark_pic(filename, default: "middleman-logo.svg")
    basename = filename || default
    title = basename.tr("_", " ").titleize
    url = "/images/#{filename}"

    image_tag url, alt: title, onerror: "this.src='images/#{default}';"
  end

  def extract_content(name)
    content = File.read(name)
    match = Middleman::Util::Data.build_regex(config[:frontmatter_delims]).match(content)
    match ? match[:additional_content] : content
  end
end

# Collections
remarks = resources.select { |r| r.ext == ".remark" && !r.data.ignore }
collection :remarks, remarks

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
remarks.each do |r|
  remark_name = File.basename(r.path)

  hljs = r.data.hljs || :default
  layout = r.data.layout || :remark
  stylesheet = r.data.stylesheet || :default
  thumbnail = r.data.thumbnail || "middleman-logo.svg"
  title = r.data.title || "Coagul"

  locals = {
    hljs: hljs,
    remark: r.source_file,
    stylesheet: stylesheet,
    thumbnail: thumbnail,
    title: title
  }

  proxy "/remarks/#{remark_name}", "/remark.html", layout: layout, locals: locals
end

proxy "/remarks/", "/index.html"
proxy "/remarks", "/index.html"

ignore "/remark.html"

# Build-specific configuration
configure :build do
  # Minify CSS on build
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript
end
